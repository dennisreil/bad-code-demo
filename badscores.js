// This code produces a runtime error.
// See if you can figure out why it's happening
// Bonus if you figure out how to fix the code to make it print the right result.
// const teamScores = {
//     "Hawks": 100,
//     "Eagles": 95,
//     "Falcons": 102
// };

// const team = "Hawks";
// const score = teamScores[team];

// score.push(101);

// console.log("The score of the Hawks is " + score);



const teamScores = {
    "Hawks": 100,
    "Eagles": 95,
    "Falcons": 102
};

// had to change the const to let in order to manipulate
// const team was redundant. wasn't necessary
let score = teamScores.Hawks;

score = 101;

console.log("The score of the Hawks is " + score);
