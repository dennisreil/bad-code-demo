// This code has a syntax error AND a logic error.
// This requires external knowledge about how discounts are
// calculated.
// See if you can fix both errors.
// const products = {
//     "Shampoo": 10,
//     "Conditioner": 8,
//     "Soap": 2
// };

// const product = "Shampoo";
// const quantity = 3;

// const discount = 0.1;  // A 10% discount
// const totalPrice = products[product] * quantity;

// if (quantity > 2) {
//     totalPrice = totalPrice - discount;
// }

// console.log("The total price is: $" + totalPrice);


// const products = {
//     "Shampoo": {
//         quantity:10,
//         price: 2.75
//     },
//     "Conditioner": {
//         quantity:8,
//         price: 2.00
//     },
//     "Soap": {
//         quantity:2,
//         price: 1.00
//     }
// };
// //lets find a way to update quantity
//  products.Shampoo.quantity = 5;
// console.log(products.Shampoo);
// // no lets initialize a variable with the quantity of product we're observing
// const quantity = products.Shampoo.quantity;

// const discount = 0.1;  // A 10% discount

// // to add a discount based on quantity, we'd need a price
// //lets next aa key with more key:values
// // then create a variable with product price
// const price = products.Shampoo.price;

// if (quantity > 2) {
//     totalPrice = products.Shampoo.price -(products.Shampoo.price * discount);
//     console.log(totalPrice);
//  } else {
//     console.log(products.Shampoo.price)
//  }

// console.log("The total price is: $" + price);



// lets try another way
const products2 = {
    "Shampoo": 10,
    "Conditioner": 8,
    "Soap": 2
};

const product2 = products2.Shampoo;
const quantity2 = 10;

const discount2 = 0.1;  // A 10% discount
let totalPrice2 = products2.Shampoo * quantity2;
console.log(totalPrice2);

if (quantity2 > 7) {
    totalPrice2 = totalPrice2 - (totalPrice2 * discount2);
    console.log("The total price is: $" + totalPrice2);
} else {
    console.log("no discount. Your final price is " + totalPrice2);
}
