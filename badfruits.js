// This file has a logic error.
// It doesn't produce a syntax error or a runtime error
// See if you can figure out what's wrong

const fruits = ["apple", "banana", "orange", "mango"];
const myFruit = "apple";

const hasFruit = fruits[myFruit];

if (hasFruit !== undefined) {
    console.log("Original Problem: I have an " + myFruit);
} else {
    console.log("Original problem: I don't have an " + myFruit);
}



// lets try this solution
const fruits2 = ["apple", "banana", "orange", "mango"];
const myFruit2 = 0;

const hasFruit2 = fruits2[myFruit2];

    if (hasFruit2 !== undefined) {
        console.log("I have an " + hasFruit2);
}       else {
            console.log("I don't have an " + hasFruit2);
}
