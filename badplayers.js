// This problem has a logic error.
// It doesn't produce a syntax error or a runtime error
// See if you can figure out what's wrong.


// const players = ["John", "Sarah", "Alex", "Mary"];
// const currentPlayer = players[4];
// console.log("The current player is " + currentPlayer);


//there is no 4 index. only up to 3
//to solve, you must either add more players, or index search what's available
const players = ["John", "Sarah", "Alex", "Mary"];
const currentPlayer = players[3];

console.log("The current player is " + currentPlayer);
