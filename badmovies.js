// This code has a logic error.
// Run it and watch what it does.
// Notice it doesn't produce an error message
// See if you can figure out where the logic error is going wrong.
// const movies = ["Inception", "Avatar", "Interstellar"];
// const ratings = {
//     "Inception": 8.8,
//     "Avatar": 7.8,

// };

// const movie = movies[2];
// const rating = ratings[movie];

// if (rating > 8.0) {
//     console.log(movie + " is highly rated.");
// } else {
//     console.log(movie + "'s rating is: " + rating);
// }


//interstellar is never called


// This code has a logic error.
// Run it and watch what it does.
// Notice it doesn't produce an error message
// See if you can figure out where the logic error is going wrong.
const movies = ["Inception", "Avatar", "Interstellar"];
const ratings = {
    "Inception": 8.8,
    "Avatar": 7.8,
    "Interstellar": 9

};

const movie = movies[0];
const rating = ratings[movie];

if (rating > 8.0) {
    console.log(movie + " is highly rated.");
} else {
    console.log(movie + "'s rating is: " + rating);
}
